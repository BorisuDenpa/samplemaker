# Sample Maker

This one Loads every image file in a folder, allows you to  
mark every point you want with a single click, and saves all  
the points in a json file.
To get to the next image simply press any key.

Intended at first for marking crowd images and using it  
with "jf to density" to generate density maps from them.