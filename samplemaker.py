'''
Loads every image from a folder, shows every image and stores the clicked
points in a json file, with the format
[{"x": 180, "y": 298}, {"x": 291, "y": 360},...]
'''
import cv2
import glob
import os
import numpy as np
import json


data = []
cv2.namedWindow('img')
# Where the images are located and where the json files will be saved
path = 'fotos/'

# Draws a circle on every click
def draw_circle(event,x,y,param,flags):
    global click, click2, img, mini, li, center
    if event == cv2.EVENT_LBUTTONDOWN:
        # Draws a circle then updates the displayed image
        cv2.circle(img, (x, y), 3, (0, 0, 255), -1)
        cv2.imshow("img", img)
        # Adds the lastest click to the output variable
        lastclick = {'x': x, 'y': y}
        data.append(lastclick)


# Sets an interrupt for the mouse
cv2.setMouseCallback('img', draw_circle)

# Loads images in a folder, to get the next image just hit any key
for img in glob.glob( path +"*.jpg"):
    # Gets the file name
    head, tail = os.path.split(img)
    name = os.path.splitext(tail)[0]
    # Displays the image
    img = cv2.imread(img)
    #cv_img.append(img)
    cv2.imshow("img", img)
    # Keeps waiting from a keystroke to load the next image, but at the same
    # time the mouse interrupt is active
    cv2.waitKey(0)

    # Saves the points in a json file with the same name as the image
    with open(path + name +'.json', 'w') as outfile:
        json.dump(data, outfile)
    # Cleans up the stored points
    data = []




